let quizzChoice;
let nbQuestion = 0;
let score = 0;

/**
 * scrol the data variable and print all descrptions
 * represented by options.
 */
function printOptions() {
    for (let index = 0; index < data.length; index++) {
        const mode = $("#select");
        const option = new Option(data[index].description, data[index].id);
        mode.append(option);
    }
}

/**
 * get the description of the chosen quiz and display it on the page.
 */
function printheme() {
    const id = new URL(window.location.href).searchParams.get("quizzId");
    for (let index = 0; index < data.length; index++) {
        if (data[index].id === id) {
            quizzChoice = index;
        }
    }
    $("#description").text(data[quizzChoice].description);
}

/**
 * print the questions of the data variable.
 */
function printQuestions() {
    $("#phrase").text(data[quizzChoice].questions[nbQuestion].question);
}

/**
 * print the current number of the question in the chosen quizz.
 */
function printNbQuestions() {
    $("#nbQst").append(nbQuestion + 1);
}

/**
 *  return que current question.
 */
function currentQuestion() {
    return data[quizzChoice].questions[nbQuestion];
}

/**
 * browse the data variable and print the questions according to the chosen quizz
 * the setence is splited and shuffled.
 */
function printWord() {
    const tab = currentQuestion().answer + " " + currentQuestion().extras;
    const wordTab = tab.split(" ");
    shuffle(wordTab);
    for (let index = 0; index < wordTab.length; index++) {
        $("#container").append($('<button>' + wordTab[index]
            + '</button>').click(moveToAnswer).addClass("proposition"));
    }
    console.log(currentQuestion().answer);
}

/**     
 ∗ Shuffle the array.
 ∗ @param {*[]} array
 */
function shuffle(array) {
    let counter = array.length;
    while (counter > 0) {
        let index = Math.floor(Math.random() * counter);
        counter--;
        [array[counter], array[index]] = [array[index], array[counter]];
    }
}

/**
 * takes the selected button and move it to the reponse area.
 */
function moveToAnswer() {
    $("#reponseArea").append($(this).off().click(moveBack));
    console.log($(this).text());
}

/**
 * takes the selected button and move it to the initial area.
 */
function moveBack() {
    $("#container").append($(this).off().click(moveToAnswer));
}

/**
 * checks if the given setence is the same as the answer.
 */
function checkIfSame() {
    $("#verif").off('click');
    $("#verif").click(function () {
        if (data[quizzChoice].questions.length - 1 == nbQuestion) {
            $("#nxtQst").hide();
        } else {
            $("#nxtQst").show();
        }
        const stringToCheck = getSentence();
        const answer = currentQuestion().answer.split(" ");
        let veri = false;
        for (let index = 0; index < answer.length; index++) {
            let same = answer.length === stringToCheck.length;
            for (let index = 0; same && index < answer.length; index++) {
                if (answer[index] !== stringToCheck[index]) {
                    same = false;
                }
            }
            if (same) {
                console.log("yes +1");
                veri = true;
                $("#verif").hide();
                $("#reponseArea >button").off();
                $("#reponseArea > button").prop("disabled", true);
                $("#container > button").prop("disabled", true);
                $("#zone").html("<span id='correct'>Bonne reponse +1</span>");
            } else {
                console.log("non , restart");
                $("#reponseArea > button").prop("disabled", true);
                $("#container > button").prop("disabled", true);
                $("#zone").html("<span id='incorrect'>Mauvaise reponse .la reponse est : " + currentQuestion().answer + "</span>");
            }
        }
        if (veri) {
            score++;
        }
        if (data[quizzChoice].questions.length - 1 == nbQuestion) {
            $("#zone").empty();
            $("#zone").html("<span id='total'>vous avez : " + score + " bonnes reponse sur " + data[quizzChoice].questions.length + "</span>");
            $("#home").show();
            $("#restart").show();
        }
    });
}

/**
 * get the sentence given by the player.
 */
function getSentence() {
    let sentence = [];
    $("#reponseArea >button").each((index, elem) => {
        sentence.push(elem.textContent);
    });
    return sentence;
}

/**
 * moves to the next question.
 */
function nextQuestion() {
    $("#nxtQst").off("click");
    if (data[quizzChoice].questions.length - 1 == nbQuestion) {
        $("#nxtQst").hide();
    }
    $("#nxtQst").click(function () {
        nbQuestion++;
        $("#container>button ").remove();
        $("#reponseArea >button").remove();
        $("#nbQst").empty();
        printOptions();
        printheme();
        printQuestions();
        printNbQuestions();
        printWord();
        checkIfSame();
        $("#zone").empty();
        nextQuestion();
        $("#verif").show();
    });
}

/**
 * execute the given fonctions when the document is ready.
 */
$(document).ready(function () {
    printOptions();
    printheme();
    printQuestions();
    printNbQuestions();
    printWord();
    checkIfSame();
    nextQuestion();
    $("#home").hide();
    $("#restart").hide();
    $("#nxtQst").hide();
});