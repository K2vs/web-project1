/**
 * scrol the data variable and print all descrptions
 * represented by options.
 */
function printOptions() {
    for (let index = 0; index < data.length; index++) {
        const mode = $("#select");
        const option = new Option(data[index].description, data[index].id);
        mode.append(option);
    }
}

$(document).ready(function () {
    printOptions();
});